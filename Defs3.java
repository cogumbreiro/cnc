import java.util.*;

class Defs3 {
  class ControlId {}
  class ItemId {}
  class StepId {}
  
  class Available<T> {
    Set<T> available = new HashSet<>();
    
    void put(T inst) {
      available.add(inst);
    }

    boolean isAvailable(T inst) {
      return available.contains(inst);
    }
  }

  enum StepStatus { INIT, DATA_READY, CONTROL_READY, READY}

  class StepState {
    StepStatus status = StepStatus.INIT;
    Set<ItemId> itemDeps = new HashSet<>();
    Set<ControlId> controlDeps = new HashSet<>();
    
    void onPutItemInst(ItemId itemInst) {
      if (itemDeps.contains(itemInst) && itemDeps.size() == 1) {
        if (status == StepStatus.INIT) {
          status = StepStatus.DATA_READY;
        } else if (status == StepStatus.CONTROL_READY) {
          status = StepStatus.READY;
        }
      }
      itemDeps.remove(itemInst);
    }
    
    void onPutControlInst(ControlId controlInst) {
      if (controlDeps.contains(controlInst) && controlDeps.size() == 1) {
        if (status == StepStatus.INIT) {
          status = StepStatus.CONTROL_READY;
        } else if (status == StepStatus.DATA_READY) {
          status = StepStatus.READY;
        }
      }
      controlDeps.remove(controlInst);
    }
    
    boolean isReady() {
      return status == StepStatus.READY;
    }
  }

  class Dependencies {
    Map<StepId, StepState> status = new HashMap<>();
    void updateDataDependencies(ItemId itemInst) {
      for (StepState s : status.values()) {
        s.onPutItemInst(itemInst);
      }
    }
    void updateControlDependencies(ControlId controlInst) {
      for (StepState s : status.values()) {
        s.onPutControlInst(controlInst);
      }
    }
    boolean isReady(StepId stepInst) {
      return status.get(stepInst).isReady();
    }
  }

  class GlobalState {
    Dependencies deps = new Dependencies();
    Available<ControlId> control = new Available<>();
    Available<ItemId> data = new Available<>();
    
    // ---- PREDICATES ----
    
    boolean isReady(StepId stepInst) {
      return deps.isReady(stepInst);
    }
    
    // ----- RULES -----
    
    boolean reducesPutItem(StepId stepInst, ItemId itemInst) {
      if (isReady(stepInst) && ! data.isAvailable(itemInst)) {
          data.put(itemInst);
          deps.updateDataDependencies(itemInst);
          return true;
      }
      return false;
    }
    
    boolean reducesGetItem(StepId stepInst, ItemId itemInst) {
      if (isReady(stepInst) && data.isAvailable(itemInst)) {
          return true;
      }
      return false;
    }
    
    boolean reducesPutControl(StepId stepInst, ControlId controlInst) {
      if (isReady(stepInst) && ! control.isAvailable(controlInst)) {
          control.put(controlInst);
          deps.updateControlDependencies(controlInst);
          return true;
      }
      return false;
    }
  }

}

