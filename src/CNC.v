Require Import Coq.Structures.OrderedType.
Require Import Coq.Structures.OrderedTypeEx.
Require Import Coq.FSets.FSetAVL.
Require Import Coq.FSets.FMapAVL.
Require Coq.FSets.FMapFacts.
Require Coq.FSets.FSetProperties.
Require Coq.FSets.FSetBridge.

Module NAME := Nat_as_OT.
Module NAME_Facts := OrderedTypeFacts NAME.
Module S := FSetAVL.Make NAME.
Module S_Props := FSetProperties.Properties S.
(*Module Set_NAME_Extra := SetUtil Set_NAME.*)
Module S_Dep := FSetBridge.DepOfNodep S.
Module M := FMapAVL.Make NAME.
Module M_Facts := FMapFacts.Facts M.
Module M_Props := FMapFacts.Properties M.

Definition name := NAME.t.
Definition set_name := S.t.

Lemma name_eq_rw:
  forall k k' : name, k = k' <-> k = k'.
Proof.
  intros.
  auto with *.
Qed.

(* --------------- *)

Definition dependencies := M.t set_name.

Definition fulfill_req t (r:dependencies) : dependencies :=
  M.mapi (fun k v => S.remove t v) r.

Structure requirements := {
  requirements_available : S.t;
  requirements_dependencies : dependencies
}.

Definition put t c :=
  Build_requirements
  (S.add t (requirements_available c))  (* collection_status *)
  (fulfill_req t (requirements_dependencies c)). (* since this tag is dead, remove it from requirements *)

Definition IsAvailable t c := S.In t (requirements_available c).

Definition IsUnavailable t c := ~ S.In t (requirements_available c).

Definition IsReady t c := exists m, M.MapsTo t m (requirements_dependencies c) /\ S.Empty m.

Definition state := (requirements *  requirements) % type.

(**
  Steps can put (tag, item) pairs into item collections and
  tags into control collections. Puts are atomic operations that
  obey the dynamic single assignment rule.


  Steps can get items from item collections by providing the tag
  of that item. A step can get an item after it has ben put.

  Steps can put tags into control collection. Putting a specific
  tag into a control collection only indicates that the steps
  prescribed by that tag will execute at some point, it does not
  have any bearing on when will those steps execute.
*)
Inductive step_op_id := PUT_ITEM | GET_ITEM | PUT_CONTROL.

Definition step_op := (name * step_op_id * name) % type.

Definition Ready t (s:state) := let (data,control) := s in IsReady t data /\ IsReady t control.

Inductive Reduces : state -> step_op -> state -> Prop :=
| reduces_put_item:
  forall data control stepInst itemInst,
  Ready stepInst (data,control) ->
  IsUnavailable itemInst data ->
  Reduces (data, control) (stepInst, PUT_ITEM, itemInst) (put itemInst data, control)
| reduces_get_item:
  forall data control stepInst itemInst,
  Ready stepInst (data,control) ->
  IsAvailable itemInst data ->
  Reduces (data, control) (stepInst, GET_ITEM, itemInst) (data, control)
| reduces_put_control:
  forall data control stepInst itemInst,
  Ready stepInst (data,control) ->
  IsUnavailable itemInst data ->
  Reduces (data, control) (stepInst, PUT_CONTROL, itemInst) (put itemInst data, control).

