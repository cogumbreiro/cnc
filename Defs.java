import java.util.*;

/**
 * We assume we know all the instances from the get go, and that no
 * new dependencies can be created.
 * No dynamic dependencies: conditional dependencies and data dependencies.
 */
class Defs {
  class ControlId {}
  class ItemId {}
  class StepId {}
  
  class Requirements<T> {
    Map<StepId, Set<T>> dependsOn = new HashMap<>();
    Set<T> available = new HashSet<>();
    
    void put(T inst) {
      fulfillReqs(inst);
      available.add(inst);
    }

    void fulfillReqs(T inst) {
      for (Set<?> x : dependsOn.values()) {
        x.remove(inst);
      }
    }
    
    boolean isAvailable(T inst) {
      return available.contains(inst);
    }

    boolean isReady(StepId stepInst) {
      return dependsOn.get(stepInst).isEmpty();
    }
  }

  class GlobalState {
    // The status of items, controls, and steps
    Requirements<ControlId> control = new Requirements<>();
    Requirements<ItemId> data = new Requirements<>();
    
    
    // ---- PREDICATES ----
    
    boolean isReady(StepId t) {
      return data.isReady(t) && control.isReady(t);
    }
    
    // ----- RULES -----
    
    boolean reducesPutItem(StepId stepInst, ItemId itemInst) {
      if (isReady(stepInst) && ! data.isAvailable(itemInst)) {
          data.put(itemInst);
          return true;
      }
      return false;
    }
    
    boolean reducesGetItem(StepId stepInst, ItemId itemInst) {
      if (isReady(stepInst) && data.isAvailable(itemInst)) {
          return true;
      }
      return false;
    }
    
    boolean reducesPutControl(StepId stepInst, ControlId controlInst) {
      if (isReady(stepInst) && ! control.isAvailable(controlInst)) {
          control.put(controlInst);
          return true;
      }
      return false;
    }
  }
  
  
}

