import java.util.*;

/**
 * We assume we know all the instances from the get go, and that no
 * new dependencies can be created.
 * No dynamic dependencies: conditional dependencies and data dependencies.
 *
 * This version separates tracking the availability of item/control instances
 * from tracking the dependencies from steps into item/control instances.
 */
class Defs2 {
  class ControlId {}
  class ItemId {}
  class StepId {}
  
  class Available<T> {
    Set<T> available = new HashSet<>();
    
    void put(T inst) {
      available.add(inst);
    }

    boolean isAvailable(T inst) {
      return available.contains(inst);
    }
  }

  class StepState {
    Set<ItemId> data = new HashSet<>();
    Set<ControlId> control = new HashSet<>();
    void updateDataDependencies(ItemId itemInst) {
      data.remove(itemInst);
    }
    void updateControlDependencies(ControlId controlInst) {
      control.remove(controlInst);
    }
    boolean isReady() {
      return data.isEmpty() && control.isEmpty();
    }
  }
  
  class Dependencies {
    Map<StepId, StepState> steps = new HashMap<>();
    void updateDataDependencies(ItemId itemInst) {
      for (StepState s : steps.values()) {
        s.updateDataDependencies(itemInst);
      }
    }
    void updateControlDependencies(ControlId controlInst) {
      for (StepState s : steps.values()) {
        s.updateControlDependencies(controlInst);
      }
    }
    boolean isReady(StepId stepInst) {
      return steps.get(stepInst).isReady();
    }
  }

  class GlobalState {
    Dependencies deps = new Dependencies();
    Available<ControlId> control = new Available<>();
    Available<ItemId> data = new Available<>();
    
    // ----- RULES -----
    
    boolean reducesPutItem(StepId stepInst, ItemId itemInst) {
      if (deps.isReady(stepInst) && ! data.isAvailable(itemInst)) {
          data.put(itemInst);
          deps.updateDataDependencies(itemInst);
          return true;
      }
      return false;
    }
    
    boolean reducesGetItem(StepId stepInst, ItemId itemInst) {
      if (deps.isReady(stepInst) && data.isAvailable(itemInst)) {
          return true;
      }
      return false;
    }
    
    boolean reducesPutControl(StepId stepInst, ControlId controlInst) {
      if (deps.isReady(stepInst) && ! control.isAvailable(controlInst)) {
          control.put(controlInst);
          deps.updateControlDependencies(controlInst);
          return true;
      }
      return false;
    }
  }
  
  
}

